**What is git**
Git is a system that helps in maintaining and creating a software. It is very useful when multiple people wants to work on single project. Basically , everyone can create a copy of pre implemented code and work on different parts and then everyone just merges it together on git to give completely working end to end project.

Repository: It is  a big box containing code of different members of the company.

Commit: It means that your are taking a snapshot of the code in repo at that moment and that snapshot will be saved in your local machine

Push: It means adding your commits to gitlab

Branches: Think of main repo as tree trunk, the branches of this tree trunk are the seprate instances of the code in repo.

Merge: It means merging the branches in master branch.

Cloning: It means copying down the whole Repository in your locol machine.

Fork: It is similar to Cloning but instead of local machine , the repo will be saved under your name in git.

